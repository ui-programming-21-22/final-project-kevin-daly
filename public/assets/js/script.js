const canvas = document.getElementById("the_canvas");
const ctx = canvas.getContext("2d");

let gameVisibility = document.getElementById("game");
let docVisibility = document.getElementById("non-game");
let savedData = document.getElementById("saved-data");

let adiv = document.getElementById('clock');
let gameOver = document.getElementById("gameover")

let doorText = document.getElementById('doorOptions');
let bedText = document.getElementById('bedOptions');
let deskText = document.getElementById('deskOptions');


let spritesheetImg = new Image();
spritesheetImg.src = "assets/img/Spritesheet.png";

let ingameDate = new Date(2020,01,01,07,00);
let startSecond =  new Date();
let previousSecond = startSecond.getSeconds();

function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

let frameNoX = 0;
let frameNoY = 0;
const scale = 3.0;
const width = 64;
const height = 64;
const scaledWidth = scale * width;
const scaledHeight = scale * height;

const roomScale = 3;
const descScale = 0.25;

let doorSpriteY = 0;

let doingActivity = false;

let blueVal = 0;

defaultChangeValue = -2;
boostedChangeValue = 5;

let energy = 200;
let energyChangeValue = defaultChangeValue;
let eRedVal = 0;
let eGreenVal = 200;
let energyUpdated = true;

let hunger = 200;
let hungerChangeValue = defaultChangeValue;
let huRedVal = 0;
let huGreenVal = 200;
let hungerUpdated = true;

let fun = 200;
let funChangeValue = defaultChangeValue;
let fRedVal = 0;
let fGreenVal = 200;
let funUpdated = true;

let bladder = 200;
let bladderChangeValue = defaultChangeValue / 2;
let bRedVal = 0;
let bGreenVal = 200;
let bladderUpdated = true;

let social = 200;
let socialChangeValue =  defaultChangeValue / 2;
let sRedVal = 0;
let sGreenVal = 200;
let socialUpdated = true;

let hygiene = 200;
let hygieneChangeValue = defaultChangeValue / 2;
let hyRedVal = 0;
let hyGreenVal = 200;
let hygieneUpdated = true;

function AnimatedGameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.frameX = width * frameNoX;
    this.frameY = height * frameNoY;
    this.Width = width;
    this.Height = height;
    this.canvasX = x;
    this.canvasY = y;
    this.scaledWidth = width * scale;
    this.scaledHeight = height * scale;
}

let player = new AnimatedGameObject(spritesheetImg, 550, 550, 64, 64);
let room = new GameObject(spritesheetImg, 0, -390, 432 * roomScale, 500 * roomScale);
let chair = new GameObject(spritesheetImg,530, 350, 134, 189);
let desk = new GameObject(spritesheetImg, 572, 280, 235, 222);
let bed = new GameObject(spritesheetImg, 976, 583, 312, 269);
let door = new GameObject(spritesheetImg, 150, 251, 118, 392);

function GamerInput(input) {
    this.action = input;
}
let gamerInput = new GamerInput("None");
let direction = new GamerInput("Left");

loadGame();

function input(event) 
{
    if (!doingActivity)
    {
        if (event.type === "keydown") 
        {
            switch (event.keyCode) 
            {
                // Keys for player
                case 37: // Left Arrow
                    gamerInput = new GamerInput("Left");
                    direction = new GamerInput("Left");
                    break;
                case 38: // Up Arrow
                    gamerInput = new GamerInput("Up");
                    direction = new GamerInput("Up");
                    break;
                case 39: // Right Arrow
                    gamerInput = new GamerInput("Right");
                    direction = new GamerInput("Right");
                    break;
                case 40: // Down Arrow
                    gamerInput = new GamerInput("Down");
                    direction = new GamerInput("Down");
                    break;

                case 49: // 1
                    if (doorText.style.visibility === "visible")
                    {
                        hygieneChangeValue = boostedChangeValue;
                        doorText.style.visibility = "hidden"
                        doingActivity = true;
                        player.canvasX = 2000;
                        doorSpriteY = 400;
                    }
                    else if (deskText.style.visibility === "visible")
                    {
                        funChangeValue = boostedChangeValue;
                        deskText.style.visibility = "hidden"
                        doingActivity = true;
                        player.canvasX = 570;
                        player.canvasY = 330;
                        directionNumber = 0;
                    }
                    else if (bedText.style.visibility === "visible")
                    {
                        energyChangeValue = boostedChangeValue;
                        bedText.style.visibility = "hidden"
                        doingActivity = true;
                        player.canvasX = 1000;
                        player.canvasY = 600;
                        directionNumber = 0;
                    }
                    break;

                case 50: // 2
                if (doorText.style.visibility === "visible")
                {
                    bladderChangeValue = boostedChangeValue;
                    doorText.style.visibility = "hidden"
                    doingActivity = true;
                    player.canvasX = 2000;
                    doorSpriteY = 400;
                }
                else if (deskText.style.visibility === "visible")
                {
                    socialChangeValue = boostedChangeValue;
                    deskText.style.visibility = "hidden"
                    doingActivity = true;
                    player.canvasX = 500;
                    player.canvasY = 300;
                    directionNumber = 0;
                }
                    break;

                case 51: // 3
                if (doorText.style.visibility === "visible")
                {
                    hungerChangeValue = boostedChangeValue;
                    doorText.style.visibility = "hidden"
                    doingActivity = true;
                    player.canvasX = 2000;
                    doorSpriteY = 400;
                    
                }
                    break;
                default:
                    gamerInput = new GamerInput("None"); //No Input 
            }
        }
        else if (event.type === "mousedown")
        {
            getCursorPosition(canvas, event);
        } 
        else 
        {
            gamerInput = new GamerInput("None");
        }
    }
    else
    {
        if (event.type === "keydown") 
        {
            if (event.keyCode == 27) // esc
            {
                doingActivity = false;
                energyChangeValue = defaultChangeValue;
                hungerChangeValue = defaultChangeValue;
                funChangeValue = defaultChangeValue;
                bladderChangeValue = defaultChangeValue / 2;
                socialChangeValue = defaultChangeValue / 2;
                hygieneChangeValue = defaultChangeValue / 2;
                player.canvasX = 550;
                player.canvasY = 550;
                doorSpriteY = 0;
            }
        }
    }
}

function getCursorPosition(canvas, event) 
{
    const rect = canvas.getBoundingClientRect()
    const x = event.clientX - rect.left
    const y = event.clientY - rect.top
    
    if (gameOver.style.visibility != "visible")
    {
        console.log("mouse:" + x + "," + y);
        console.log("door:" + door.x + "," + door.y);
        if(x > door.x && x < (door.x + door.width) && y > door.y && y < door.y + door.height)
        {
            //console.log("door:" + door.x + "," + door.y + "mouse:" + x + "," + y);
            //console.log("mouse:" + x + "," + y);
            doorText.style.visibility = "visible";
            bedText.style.visibility = "hidden";
            deskText.style.visibility = "hidden";
        }
        else if(x > bed.x && x < (bed.x + bed.width) && y > bed.y && y < bed.y  + bed.height)
        {
            bedText.style.visibility = "visible";
            doorText.style.visibility = "hidden";
            deskText.style.visibility = "hidden";
        }
        else if((x > desk.x && x < (desk.x + desk.width) && y > desk.y && y < desk.y  + desk.height) ||
                (x > chair.x && x < (chair.x + chair.width) && y > chair.y && y < chair.y  + chair.height))
        {
            bedText.style.visibility = "hidden";
            doorText.style.visibility = "hidden";
            deskText.style.visibility = "visible";
        }
        else
        {
            doorText.style.visibility = "hidden";
            bedText.style.visibility = "hidden";
            deskText.style.visibility = "hidden";
        }
    }
}

let speed = 10;

const walkLoop = [1,2,3,4,5,6,7,8];
let currentLoopIndex = 0;
let frameCount = 0;
let directionNumber = 2;
let playerScale = 1;
let frameSpeed = 4;

function update()
{
    if (gameOver.style.visibility != "visible")
    {
        clock()
    }

    updateEnergyBar() 
    updateHungerBar() 
    updateFunBar() 
    updateBladderBar() 
    updateSocialBar() 
    updateHygieneBar() 

    // move player
    if (gamerInput.action === "Up") {
        console.log("Move Up");
        player.canvasY -= speed;
        directionNumber = 0;
        frameCount++;
    } else if (gamerInput.action === "Down") {
        console.log("Move Down");
        player.canvasY += speed;
        directionNumber = 2;
        frameCount++;
    } else if (gamerInput.action === "Left") {
        console.log("Move Left");
        player.canvasX -= speed;
        directionNumber = 1;
        playerScale = 1;
        frameCount++;
    } else if (gamerInput.action === "Right") {
        console.log("Move Right");
        player.canvasX += speed;
        directionNumber = 3;
        playerScale = -1
        frameCount++;
    }
    else if (gamerInput.action != "Up" &&
             gamerInput.action != "Down" &&
             gamerInput.action != "Right" &&
             gamerInput.action != "Left") 
    {
        player.frameX = 0
        player.frameY = directionNumber;
        frameCount = frameSpeed - 1;
    }

    // boundary check for player
    if (player.canvasY <= 300 && player.canvasX != 2000)
    {
        player.canvasY += speed;
    }
    if (player.canvasY >= 1080 - player.scaledHeight * 1.5 && player.canvasX != 2000)
    {
        player.canvasY -= speed;
    }
    if (player.canvasX <= -6 && player.canvasX != 2000)
    {
        player.canvasX += speed;
    }
    if (player.canvasX >= 1320 - player.scaledWidth && player.canvasX != 2000)
    {
        player.canvasX -= speed;
    }

    if (frameCount >= frameSpeed)
    {
        console.log (walkLoop[currentLoopIndex]);
        player.frameX = walkLoop[currentLoopIndex];
        player.frameY = directionNumber;
        currentLoopIndex++;
        if (currentLoopIndex >= walkLoop.length) {
          currentLoopIndex = 0;
          console.log("changing direction to dir: " + direction.action);
        }
        frameCount = 0;
    }
    
    let modifierA = 0;
    let modifierB = 0;
    let modifierC = 0;
    defaultChangeValue = -2
    if (bladder <= 100)
    {
        defaultChangeValue += -1
    }
    if (social <= 100)
    {
        defaultChangeValue += -1
    }
    if (hygiene <= 100)
    {
        defaultChangeValue += -1
    }

    boostedChangeValue = 5;
    if (bladder > 100)
    {
        boostedChangeValue += 1
    }
    if (social > 100)
    {
        boostedChangeValue += 1
    }
    if (hygiene > 100)
    {
        boostedChangeValue += 1
    }

    if (energy <= 0 || hunger <= 0 || fun <= 0)
    {
        gameOver.style.visibility = "visible";
    }

}

// updates the status bar for energy
function updateEnergyBar() 
{
    let energyBar = document.getElementById("energy");
    if (ingameDate.getMinutes() % 2 == 0)
    {
        if (!energyUpdated && energy + energyChangeValue <= 200)
        {
            eRedVal -= energyChangeValue;
            eGreenVal += energyChangeValue;
            energyBar.style.backgroundColor = "rgb(" + eRedVal + ", " + eGreenVal + ", " + blueVal + ")";
            energy += energyChangeValue;
            energyBar.style.width = energy + "px";
            energyUpdated = true;
        }
        else if (!energyUpdated && energy + energyChangeValue > 200)
        {
            eRedVal = 0;
            eGreenVal = 200;
            energyBar.style.backgroundColor = "rgb(" + eRedVal + ", " + eGreenVal + ", " + blueVal + ")";
            energy = 200;
            energyBar.style.width = energy + "px";
            energyUpdated = true;
        }
        
    }
    else
    {
        energyUpdated = false;
    }
}

// updates the status bar for Hunger
function updateHungerBar() 
{
    let hungerBar = document.getElementById("hunger");
    if (ingameDate.getMinutes() % 2 == 0)
    {
        if (!hungerUpdated && hunger + hungerChangeValue <= 200)
        {
            huRedVal -= hungerChangeValue;
            huGreenVal += hungerChangeValue;
            hungerBar.style.backgroundColor = "rgb(" + huRedVal + ", " + huGreenVal + ", " + blueVal + ")";
            hunger += hungerChangeValue;
            hungerBar.style.width = hunger + "px";
            hungerUpdated = true;
        }
        else if (!hungerUpdated && hunger + hungerChangeValue > 200)
        {
            huRedVal = 0;
            huGreenVal = 200;
            hungerBar.style.backgroundColor = "rgb(" + huRedVal + ", " + huGreenVal + ", " + blueVal + ")";
            hunger = 200;
            hungerBar.style.width = hunger + "px";
            hungerUpdated = true;
        }
    }
    else
    {
        hungerUpdated = false;
    }
}

// updates the status bar for Fun
function updateFunBar() 
{
    let funBar = document.getElementById("fun");
    if (ingameDate.getMinutes() % 2 == 0)
    {
        if (!funUpdated && fun + funChangeValue <= 200)
        {
            fRedVal -= funChangeValue;
            fGreenVal += funChangeValue;
            funBar.style.backgroundColor = "rgb(" + fRedVal + ", " + fGreenVal + ", " + blueVal + ")";
            fun += funChangeValue;
            funBar.style.width = fun + "px";
            funUpdated = true;
        }
        else if (!funUpdated && fun + funChangeValue > 200)
        {
            fRedVal = 0;
            fGreenVal = 200;
            funBar.style.backgroundColor = "rgb(" + fRedVal + ", " + fGreenVal + ", " + blueVal + ")";
            fun = 200;
            funBar.style.width = fun + "px";
            funUpdated = true;
        }
    }
    else
    {
        funUpdated = false;
    }
}

// updates the status bar for bladder
function updateBladderBar() 
{
    let bladderBar = document.getElementById("bladder");
    if (ingameDate.getMinutes() % 2 == 0)
    {
        if (!bladderUpdated && bladder + bladderChangeValue <= 200)
        {
            bRedVal -= bladderChangeValue;
            bGreenVal += bladderChangeValue;
            bladderBar.style.backgroundColor = "rgb(" + bRedVal + ", " + bGreenVal + ", " + blueVal + ")";
            bladder += bladderChangeValue;
            bladderBar.style.width = bladder + "px";
            bladderUpdated = true;
        }
        else if (!bladderUpdated && bladder + bladderChangeValue > 200)
        {
            bRedVal = 0;
            bGreenVal = 200;
            bladderBar.style.backgroundColor = "rgb(" + bRedVal + ", " + bGreenVal + ", " + blueVal + ")";
            bladder = 200;
            bladderBar.style.width = bladder + "px";
            bladderUpdated = true;
        }
    }
    else
    {
        bladderUpdated = false;
    }
}

// updates the status bar for social
function updateSocialBar() 
{
    let socialBar = document.getElementById("social");
    if (ingameDate.getMinutes() % 2 == 0)
    {
        if (!socialUpdated && social + socialChangeValue <= 200)
        {
            sRedVal -= socialChangeValue;
            sGreenVal += socialChangeValue;
            socialBar.style.backgroundColor = "rgb(" + sRedVal + ", " + sGreenVal + ", " + blueVal + ")";
            social += socialChangeValue;
            socialBar.style.width = social + "px";
            socialUpdated = true;
        }
        else if (!socialUpdated && social + socialChangeValue > 200)
        {
            sRedVal = 0;
            sGreenVal = 200;
            socialBar.style.backgroundColor = "rgb(" + sRedVal + ", " + sGreenVal + ", " + blueVal + ")";
            social = 200;
            socialBar.style.width = social + "px";
            socialUpdated = true;
        }
    }
    else
    {
        socialUpdated = false;
    }
}
  
// updates the status bar for hygiene
function updateHygieneBar() 
{
    let hygieneBar = document.getElementById("hygiene");
    if (ingameDate.getMinutes() % 2 == 0)
    {
        if (!hygieneUpdated && hygiene + hygieneChangeValue <= 200)
        {
            hyRedVal -= hygieneChangeValue;
            hyGreenVal += hygieneChangeValue;
            hygieneBar.style.backgroundColor = "rgb(" + hyRedVal + ", " + hyGreenVal + ", " + blueVal + ")";
            hygiene += hygieneChangeValue;
            hygieneBar.style.width = hygiene + "px";
            hygieneUpdated = true;
        }
        else if (!hygieneUpdated && hygiene + hygieneChangeValue > 200)
        {
            hyRedVal = 0;
            hyGreenVal = 200;
            hygieneBar.style.backgroundColor = "rgb(" + hyRedVal + ", " + hyGreenVal + ", " + blueVal + ")";
            hygiene = 200;
            hygieneBar.style.width = hygiene + "px";
            hygieneUpdated = true;
        }
    }
    else
    {
        hygieneUpdated = false;
    }
}

function clock()
{
    let date = new Date();

    if (ingameDate.getHours() < 10 && ingameDate.getMinutes() < 10)
    {
        adiv.innerHTML = ("0" + ingameDate.getHours()
        +":0"+ingameDate.getMinutes()
        +"<br>" 
        + ingameDate.getDate()
        +"/"+ingameDate.getMonth()
        +"/"+ingameDate.getFullYear());
    }
    else if (ingameDate.getHours() < 10)
    {
        adiv.innerHTML = ("0" + ingameDate.getHours()
        +":"+ingameDate.getMinutes()
        +"<br>" 
        + ingameDate.getDate()
        +"/"+ingameDate.getMonth()
        +"/"+ingameDate.getFullYear());
    }
    else if (ingameDate.getMinutes() < 10)
    {
        adiv.innerHTML = (ingameDate.getHours()
        +":0"+ingameDate.getMinutes()
        +"<br>" 
        + ingameDate.getDate()
        +"/"+ingameDate.getMonth()
        +"/"+ingameDate.getFullYear());
    }
    else
    {
        adiv.innerHTML = (ingameDate.getHours()
        +":"+ingameDate.getMinutes()
        +"<br>" 
        + ingameDate.getDate()
        +"/"+ingameDate.getMonth()
        +"/"+ingameDate.getFullYear());
    }

    if (date.getSeconds() > previousSecond || (date.getSeconds() == 0 && previousSecond == 59))
    {
        /*console.log("prev: " + previousSecond + "  date: " + date.getSeconds());*/
        ingameDate.setMinutes(ingameDate.getMinutes() + 1);
        previousSecond = date.getSeconds();
    }

    if (ingameDate.getMinutes() == 59)
    {
        localStorage.setItem("date&time", ingameDate);
        localStorage.setItem("energy", energy);
        localStorage.setItem("hunger", hunger);
        localStorage.setItem("fun", fun);
        localStorage.setItem("bladder", bladder);
        localStorage.setItem("social", social);
        localStorage.setItem("hygiene", hygiene);
        localStorage.removeItem("played");
    }
}

/*
let dynamic = nipplejs.create({
    zone: document.getElementById('joystick_zone'),
    color: 'red',
});

dynamic.on('added', function (event, joyStick) 
{
    console.log("Created joystick");

    joyStick.on('dir:up', function(event, data)
    {
        console.log("UP");
    });
    joyStick.on('dir:down', function(event, data)
    {
        console.log("DOWN");
    });
    joyStick.on('dir:left', function(event, data)
    {
        console.log("LEFT");
    });
    joyStick.on('dir:right', function(event, data)
    {
        console.log("RIGHT");
    });
    joyStick.on('end', function(event, data)
    {
        console.log("none");
    });
});
*/

function validateForm(){
    console.log("called validateForm!");
    if (localStorage.getItem("date&time") == undefined)
    {
        let name = document.forms["FirstRunForm"]["userName"].value;
        if(name === "")
        {
            alert("Enter thy name!");
            return false;
        }
        else{
            alert("Howdy " + name);
            localStorage.setItem("name", name);
            loadGame();
        }
    }
}

function validateSaveForm(){
    let answer = document.querySelector('#yes');
    if(answer.checked == true)
    {
        energy = localStorage.getItem("energy");  
        hunger = localStorage.getItem("hunger");
        fun = localStorage.getItem("fun");
        bladder = localStorage.getItem("bladder");
        social = localStorage.getItem("social");
        hygiene = localStorage.getItem("hygiene");
        
        alert("yes!");
    }
    answer = document.querySelector('#no');
    if(answer.checked == true)
    {
        localStorage.removeItem("date&time")
        localStorage.removeItem("name")
        localStorage.removeItem("energy")
        localStorage.removeItem("hunger");
        localStorage.removeItem("fun");
        localStorage.removeItem("bladder");
        localStorage.removeItem("social");
        localStorage.removeItem("hygiene");

        alert("no!");
    }
}

function loadGame(){
    let queryString = document.location.search;
    if (queryString.includes("&"))
    {
        queryString = queryString.split("&")[0];
    }
    let name = queryString.split("=")[1];

    if (localStorage.getItem("name") == undefined)
    {
        docVisibility.style.visibility = "visible";
        savedData.style.visibility = "hidden";
        gameVisibility.style.visibility = "hidden";
    }
    /*
    else if (localStorage.getItem("date&time") != undefined && localStorage.getItem("played") == undefined)
    {
        docVisibility.style.visibility = "hidden";
        savedData.style.visibility = "visible";
        gameVisibility.style.visibility = "hidden";
    }
    */
    else
    {
        docVisibility.style.visibility = "hidden";
        savedData.style.visibility = "hidden";
        gameVisibility.style.visibility = "visible";
        localStorage.setItem("played", "yes");
    }
}

function loop()
{
    if (gameVisibility.style.visibility === "visible")
    {
        update();
        draw();
        window.requestAnimationFrame(loop)
    }

}

function draw() 
{
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.drawImage(room.spritesheet, 
        0,
        512,
        432,
        500,
        room.x,
        room.y,
        room.width,
        room.height);

    ctx.drawImage(desk.spritesheet, 
        576,
        272,
        235,
        222,
        desk.x,
        desk.y,
        desk.width,
        desk.height);

    ctx.drawImage(chair.spritesheet,
        576,
        496,
        134,
        189,
        chair.x,
        chair.y,
        chair.width,
        chair.height);

    ctx.drawImage(bed.spritesheet, 
        576,
        0, 
        312,
        269,
        bed.x,
        bed.y,
        bed.width,
        bed.height);  

    ctx.drawImage(door.spritesheet, 
        895,
        doorSpriteY,
        113,
        396,
        door.x,
        door.y,
        door.width,
        door.height);  

    ctx.drawImage(player.spritesheet,
        player.frameX * width, player.frameY * height, width, height,
        player.canvasX, player.canvasY, scaledWidth, scaledHeight);
}

window.requestAnimationFrame(loop);

window.addEventListener('keydown', input);
window.addEventListener('keyup', input);
window.addEventListener('mousedown', input);
window.addEventListener('mouseup', input);